﻿using School.Domain.Entities;
using System.Collections.Generic;

namespace School.Domain.Interfaces.Repository
{
    public interface IDepartmentRepository : IRepositoryBase<Department>
    {
        List<Department> GetDepartments();
        Department GetDepartmentById(int id);
    }
}
