﻿using School.Domain.Entities;
using System.Collections.Generic;

namespace School.Domain.Interfaces.Repository
{
    public interface ICourseRepository : IRepositoryBase<Course>
    {
        List<Course> GetCoursesByDepartment(int departmentId);
        List<Course> GetCourses();
        Course GetCourse(int courseId);
    }
}
