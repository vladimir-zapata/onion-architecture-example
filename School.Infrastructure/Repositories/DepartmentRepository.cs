﻿

using Microsoft.Extensions.Logging;
using School.Domain.Entities;
using School.Domain.Interfaces.Repository;
using School.Infrastructure.Context;
using School.Infrastructure.Core;
using School.Infrastructure.Exceptions;
using School.Infrastructure.Models;
using System;
using System.Collections.Generic;
using System.Linq;

namespace School.Infrastructure.Repositories
{
    public class DepartmentRepository : BaseRepository<Department>, IDepartmentRepository
    {
        private readonly SchoolContext context;
        private readonly ILogger<DepartmentRepository> logger;

        public DepartmentRepository(SchoolContext context,
                                    ILogger<DepartmentRepository> logger) : base(context)
        {
            this.context = context;
            this.logger = logger;
        }

        public override void Add(Department entity)
        {

            if (this.Exists(dep => dep.Name == entity.Name))
            {
                throw new DepartmentException("Departamento existe.");
            }

            base.Add(entity);
            base.SaveChanges();
        }
             
        public override void Update(Department entity)
        {
            try
            {

                
                Department departmentToUpdate = this.GetEntity(entity.DepartmentID);


                //entity.GetType().GetProperties().ToList().ForEach(cd => {

                //    var propertyValue = cd.GetValue(entity);

                //    departmentToUpdate.GetType()
                //                      .GetProperty(cd.Name)
                //                      .SetValue(departmentToUpdate, 
                //                                propertyValue,
                //                                null);
                //});


                departmentToUpdate.DepartmentID = entity.DepartmentID;
                departmentToUpdate.ModifyDate = entity.ModifyDate;
                departmentToUpdate.Name = entity.Name;
                departmentToUpdate.StartDate = entity.StartDate;
                departmentToUpdate.UserMod = entity.UserMod;
                departmentToUpdate.Administrator = entity.Administrator;
                departmentToUpdate.Budget = entity.Budget;


                this.context.Departments.Update(departmentToUpdate);
                this.context.SaveChanges();

            }
            catch (Exception ex)
            {

                this.logger.LogError("Error actualizando el departamento", ex.ToString());
            }
        }
        public override void Remove(Department entity)
        {
            try
            {
                Department departmentToRemove= this.GetEntity(entity.DepartmentID);

                departmentToRemove.Deleted = entity.Deleted;
                departmentToRemove.DeletedDate = entity.DeletedDate;
                departmentToRemove.UserDeleted = entity.UserDeleted;

                this.context.Departments.Update(departmentToRemove);
                this.context.SaveChanges();

            }
            catch (Exception ex)
            {

                this.logger.LogError("Error eliminando el departamento", ex.ToString());
            }
        }
        public Department GetDepartmentById(int id)
        {
            Department department = new Department();


            try
            {
                department = this.GetEntity(id);

            }
            catch (Exception ex)
            {

                this.logger.LogError("Error obteniendo el department", ex.ToString());
            }

            return department;
        }
        public List<Department> GetDepartments()
        {

            List<Department> departments = new List<Department>();

            try
            {
                departments = this.context.Departments
                                 .Where(cd => !cd.Deleted)
                                 .ToList();
            }
            catch (Exception ex)
            {

                this.logger.LogError("Error obteniendo los departamentos", ex.ToString());
            }

            return departments;
        }

    }
}
