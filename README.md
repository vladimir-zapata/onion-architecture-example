**Core (`School.Domain`):**
- Core should have no dependencies on any other layer. It contains the core business entities and business logic, serving as the innermost and most important layer.

**Application (`School.Application`):**
- Application layer depends on the Core (`School.Domain`) layer as it implements application-specific business rules and use cases that leverage the core domain entities and logic. The Application layer should not depend on other outer layers.

**Infrastructure (`School.Infrastructure`):**
- Infrastructure layer depends on the Core (`School.Domain`) layer to provide concrete implementations for interacting with external systems (e.g., databases, UI frameworks, APIs). The Infrastructure layer should not depend on other outer layers.

**School.IOC (Dependency Injection):**
- In Onion Architecture, the Inversion of Control (IoC) container or Dependency Injection (DI) configuration is often a separate project that manages the dependencies. It should depend on both the Application (`School.Application`) and Infrastructure (`School.Infrastructure`) layers to wire them together.

**Presentation (`School.Api`, `School.Lambda`, `School.SPA`, `School.Web`):**
- The Presentation layer represents the user interfaces or APIs and depends on the Application (`School.Application`) layer to interact with the core domain entities and use cases. It should also depend on the `School.IOC` project to access the configured dependencies.

**Test (`School.Test`):**
- The Test project can reference all the other projects (Core, Application, Infrastructure, Presentation, and `School.IOC`) for testing purposes. It should not introduce circular dependencies between the other projects.

